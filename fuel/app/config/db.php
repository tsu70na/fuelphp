<?php
/**
 * Use this file to override global defaults.
 *
 * See the individual environment DB configs for specific config information.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        =>'mysql:unix_socket',
			'mysql:unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock/fuelphp_dev',
			'username'   => 'root',
			'password'   => 'root',
		),
	),
);
