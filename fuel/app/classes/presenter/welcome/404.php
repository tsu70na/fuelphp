<?php

/**
 * The welcome 404 presenter.
 *
 * @package  app
 * @extends  Presenter
 */
class Presenter_Welcome_404 extends Presenter
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 *
	 * @return void
	 */
	public function view()
	{
		$messages = array('うんこ＝マザーファッカーなんだよ♡!', 'ごりら渾身の求愛「ドラミング」！！！！！', 'ああああああああ...あん♡（810先輩）!');
		$this->title = $messages[array_rand($messages)];
	}
}
